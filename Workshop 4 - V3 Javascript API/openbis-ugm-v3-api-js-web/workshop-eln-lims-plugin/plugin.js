/*
 * Interesting helper classes to look at
 * js/util/FormUtil.js
 * js/util/Util.js
 * js/server/ServerFacade.js
 */
function WorkshopTechnology() {
	this.init();
}

$.extend(WorkshopTechnology.prototype, ELNLIMSPlugin.prototype, {
	init: function() {

	},
	forcedDisableRTF : [],
	forceMonospaceFont : [],
	sampleTypeDefinitionsExtension : {

	},
	dataSetTypeDefinitionsExtension : {

	},
	sampleFormTop : function($container, model) {
        var $thinkAboutMe = FormUtil.getButtonWithText("What do you think about me?", function() {
                mainController.serverFacade.customASService({
                    "method" : "echo",
                    "message" : "You are awesome!"
                }, function(serviceOutput) {
                    Util.showSuccess(serviceOutput.result);
                }, "workshop-custom-as-api");
        }, '', null);

        $container.append($thinkAboutMe);
	},
	sampleFormBottom : function($container, model) {

	},
	dataSetFormTop : function($container, model) {

	},
	dataSetFormBottom : function($container, model) {

	}
});

profile.plugins.push(new WorkshopTechnology());