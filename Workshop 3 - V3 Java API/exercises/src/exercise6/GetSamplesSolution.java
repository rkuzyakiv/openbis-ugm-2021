package exercise6;

import java.util.List;
import java.util.Map;

import ch.ethz.sis.openbis.generic.asapi.v3.IApplicationServerApi;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.Sample;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.fetchoptions.SampleFetchOptions;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.id.ISampleId;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.id.SampleIdentifier;
import common.Openbis;

public class GetSamplesSolution
{
    public static void main(String[] args)
    {
        // EXERCISE
        //
        // Get samples with the following identifiers:
        //
        //      /WORKSHOP_V3_JAVA_API/SWITZERLAND/SANTIS
        //      /WORKSHOP_V3_JAVA_API/SWITZERLAND/EIGER
        //      /WORKSHOP_V3_JAVA_API/SWITZERLAND/ZERMATT
        //
        // The samples should be fetched with their experiments (lands) and properties (elevations).
        // Print the fetched data to the console.

        IApplicationServerApi v3 = Openbis.createApplicationServerApi();
        String sessionToken = v3.login(Openbis.USER, Openbis.PASSWORD);

        List<ISampleId> ids = List.of(new SampleIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/SANTIS"),
                new SampleIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/EIGER"), new SampleIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/ZERMATT"));

        SampleFetchOptions fo = new SampleFetchOptions();
        fo.withExperiment();
        fo.withProperties();

        Map<ISampleId, Sample> samples = v3.getSamples(sessionToken, ids, fo);

        for (Sample sample : samples.values())
        {
            System.out.println(
                    "Sample: " + sample.getIdentifier() + ", experiment: " + sample.getExperiment().getIdentifier() + ", properties:" + sample
                            .getProperties());
        }

        v3.logout(sessionToken);
    }
}
